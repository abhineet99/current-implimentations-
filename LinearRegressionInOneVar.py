#Data visual
import numpy as np
x,y=np.loadtxt('ex1data1.txt', delimiter=',', unpack=True)

import matplotlib.pyplot as plt
plt.plot(x,y, 'rx')
plt.xlabel('Population of cities in 10,000s')
plt.ylabel('Profit in $10,000')
plt.title('Data Visualization')
plt.show()

#checking J(0) 
import numpy as np
import matplotlib.pyplot as plt 
x,y=np.loadtxt('ex1data1.txt', delimiter=',', unpack=True)
m=len(y)
a=np.ones(m, dtype=int)
b=np.loadtxt('ex1data1.txt', delimiter=',', unpack=True, usecols=0)
X=np.vstack((a,b))
Y=np.loadtxt('ex1data1.txt', delimiter=',', unpack=True, usecols=1)

print('Enter 0o and 01')
t0=int(input())
t1=int(input())

def predict(x):
	return float(t1)*float(x)+float(t0)
def loss(y_pred, y):
	return np.power(y_pred-y, 2)

L=0

for x, y in zip(b, Y):
	y_pred=predict(x)
	L+=loss(y_pred,y)
J=L/(2*m)
print('Cost computed =', J)

#Visualizing J(0)
from mpl_toolkits.mplot3d import axes3d
J=[]
te0=[]
te1=[]
for t0 in np.arange(-10, 10, 0.5):
	for t1 in np.arange(-10, 10, 0.5):
		L=0
		for x, y in zip(b, Y):
			y_pred=predict(x)
			L+=loss(y_pred, y)
		temp=L/(2*m)
		J.append(temp)
		te0.append(t0)
		te1.append(t1)	

import matplotlib.pyplot as plt
fig=plt.figure()
ax1=fig.add_subplot(111, projection='3d')
ax1.scatter(te0, te1, J)
plt.show()
x,y=np.loadtxt('ex1data1.txt', delimiter=',', unpack=True)


#Gradient Decent
print('Enter base theta0, base theta1, learning rate')
b=np.loadtxt('ex1data1.txt', delimiter=',', unpack=True, usecols=0)
Y=np.loadtxt('ex1data1.txt', delimiter=',', unpack=True, usecols=1)
t0=float(input('t0='))
t1=float(input('t1='))
a=float(input('a='))

def predict(x_value):
	return t1*x_value+t0

for epoch in range(1500):
	for x, y in zip(b, Y):
		y_pred=predict(x)
		gradient0=2*(y_pred-y)
		gradient1=2*(y_pred-y)*x
		t0-=a*gradient0
		t1-=a*gradient1

print('0o ,01 =', t0, t1)

#graph of data and line 
import matplotlib.pyplot as plt
import numpy as np
x1, y1=np.loadtxt('ex1data1.txt', delimiter=',', unpack=True)
y2=[(t1*x+t0) for x in x1]
plt.plot(x1, y1, 'rx', label='data given')
plt.plot(x1, y2, 'b', label='linear regression line')
plt.legend()
plt.show()