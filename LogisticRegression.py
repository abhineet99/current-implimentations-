#Graphing given data
import matplotlib.pyplot as plt
import numpy as np
X=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=0)
x=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=1)
y=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=2)
x1=[]
x2=[]
X1=[]
X2=[]
for x,y,z in zip(X,x,y):
	if (z==0):
		X1.append(x)
		X2.append(y)
	elif (z==1):
		x1.append(x)
		x2.append(y)

plt.plot(X1, X2, 'rx', label='Not Admitted')
plt.plot(x1, x2, 'bo', label='Admitted')
plt.legend()
plt.show()

#checking J(0) 
X=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=0)
x=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=1)
y=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=2)
print('Enter 0o, 01 and 02')
t0=float(input())
t1=float(input())
t2=float(input())
m=len(X)
def predict(x_1, x_2):
	temp=float(t1)*float(x_1)+float(t2)*float(x_2)+float(t0)
	return 1/(1+np.exp(-temp))
l=0
for a, b, c in zip(X, x, y):
	h=predict(a, b)
	l+=-1*c*np.log(h)-(1-c)*np.log(1-h)
J=l/m
print('Computed cost =', J)

#Gradient Decent 
X=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=0)
x=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=1)
y=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=2)
print('Enter base t0, base t1, base t2 and  learning rate')
t0=float(input('t0='))
t1=float(input('t1='))
t2=float(input('t2='))
v=float(input('a='))
i=0
for epoch in range(2290):
	l=0
	for d, e, f in zip(X, x, y):
		h=predict(d, e)
		gradient0=2*(h-f)
		gradient1=2*(h-f)*d
		gradient2=2*(h-f)*e
		t0-=v*gradient0
		t1-=v*gradient1
		t2-=v*gradient2

print('0o, 01, 02 =', t0, t1, t2)
l=0
for a, b, c in zip(X, x, y):
	h=predict(a, b)
	l+=-1*c*np.log(h)-(1-c)*np.log(1-h)
J=l/m
print('Computed cost =', J)

#Output classifier 
X=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=0)
x=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=1)
Y=[]
for p, q in zip(X, x):
	h=float(t1)*float(p)+float(t2)*float(q)+float(t0)
	if (h>=float(0)):
		out=1
	if (h<float(0)):
		out=0
	Y.append(out)

#Check accuracy of hypothysis
s=0
for y1, y2 in zip(Y, y):
	temp=y1-y2
	if (temp==0):
		s=s+1
acc=s/len(Y)
print('accuracy=', acc*100)

#Plotting line
X=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=0)
x=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=1)
y=np.loadtxt('ex2data1.txt', delimiter=',', unpack=True, usecols=2)
x1=[]
x2=[]
X1=[]
X2=[]
for x,y,z in zip(X,x,y):
	if (z==0):
		X1.append(x)
		X2.append(y)
	elif (z==1):
		x1.append(x)
		x2.append(y)

plt.plot(X1, X2, 'rx', label='Not Admitted')
plt.plot(x1, x2, 'bo', label='Admitted')
t=[30, 40, 50, 60, 70]
k=[((t1*f+t0)/(-t2)) for f in t]
plt.plot(t, k, 'g', label='Decision Boundry')
plt.legend()
plt.show()	